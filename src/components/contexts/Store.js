import {createContext, useReducer} from 'react';
import Reducer from './Reducer';

const initalState = {
  userData: {
    isonBoarding: false,
  },
};

const Store = ({children}) => {
  const [state, dispatch] = useReducer(Reducer, initalState);
  return (
    <Context.Provider value={{state, dispatch}}>{children}</Context.Provider>
  );
};

export const Context = createContext(initalState);
export default Store;
