import {StyleSheet, Text, View, Dimensions} from 'react-native';
import React from 'react';

const {width, height} = Dimensions.get('window');

// constants
import {SIZES, SIZE, FontFamily} from '../../../constants/Constants';

// packages
import Lottie from 'lottie-react-native';

const SlideItem = ({data}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>{data.title}</Text>
      <View style={styles.lottieView}>
        <Lottie
          source={data.background}
          width={'100%'}
          height={'100%'}
          resizeMode="cover"
          autoPlay
          loop
        />
      </View>
    </View>
  );
};

export default SlideItem;

const styles = StyleSheet.create({
  container: {
    width: width,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleText: {
    fontSize: SIZE(30),
    fontFamily: FontFamily.Bold,
    color: '#1879FB',
  },
  lottieView: {
    width: SIZES.WP('70%'),
    height: SIZES.WP('70%'),
  },
});
