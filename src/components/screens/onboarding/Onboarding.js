import {FlatList, StyleSheet, Text, View} from 'react-native';
import React, {useState, useRef, useContext} from 'react';

// components
import SlideItem from './includes/SlideItem';
import CommanButton from '../../includes/CommanButton';

// constants
import {SIZE, SIZES} from '../../constants/Constants';

// context
import {Context} from '../../contexts/Store';

const Onboarding = () => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const slidesRef = useRef(null);

  const {state, dispatch} = useContext(Context); // context

  // function for scroll onboarding page
  const handleScroll = event => {
    const {contentOffset} = event.nativeEvent;
    const viewSize = event.nativeEvent.layoutMeasurement;
    const index = Math.floor(contentOffset.x / viewSize.width);
    setCurrentIndex(index);
  };

  // function for buttonSubmit
  const buttonSubmit = () => {
    dispatch({
      type: 'UPDATE_USER_DATA',
      userData: {
        isonBoarding: true,
      },
    });
  };

  const slides = [
    {
      id: 1,
      title: 'Send Free Message',
      background: require('../../../assets/lotties/lottie-one.json'),
    },
    {
      id: 2,
      title: 'Connect Your Friend',
      background: require('../../../assets/lotties/lottie-two.json'),
    },
    {
      id: 3,
      title: 'Welcome',
      background: require('../../../assets/lotties/lottie-four.json'),
    },
  ];
  return (
    <View style={styles.container}>
      <FlatList
        data={slides}
        renderItem={({item}) => <SlideItem key={item.id} data={item} />}
        onScroll={handleScroll}
        ref={slidesRef}
        horizontal
        showsHorizontalScrollIndicator={false}
        pagingEnabled={true}
        bounces={false}
        keyExtractor={item => item.id}
        scrollEventThrottle={32}
      />
      <View style={styles.bottomView}>
        <View style={styles.slideView}>
          {slides.map((item, index) => (
            <View
              key={item.id}
              style={[
                styles.slide,
                {
                  marginRight: slides.length === item.id ? 0 : SIZE(8),
                  backgroundColor:
                    currentIndex === index ? '#1879FB' : '#D9D9D9',
                  width: currentIndex === index ? SIZE(40) : SIZE(24),
                },
              ]}></View>
          ))}
        </View>
        <CommanButton title={'Get started'} buttonSubmit={buttonSubmit} />
      </View>
    </View>
  );
};

export default Onboarding;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingBottom: SIZE(60),
  },
  bottomView: {
    paddingHorizontal: SIZE(35),
  },
  slideView: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: SIZE(60),
  },
  slide: {
    height: SIZE(8),
    borderRadius: 8,
  },
});
