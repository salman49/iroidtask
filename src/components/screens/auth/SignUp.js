import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';

// constants
import {FontFamily, SIZE, SIZES} from '../../constants/Constants';

// assets
import MailIcon from '../../../assets/icons/mailicon.svg';
import LockIcon from '../../../assets/icons/lockicon.svg';

// packages
import Lottie from 'lottie-react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

// components
import CommanButton from '../../includes/CommanButton';

const SignUp = () => {
  return (
    <KeyboardAwareScrollView contentContainerStyle={styles.container}>
      <View style={styles.topView}>
        <View style={styles.lottieView}>
          <Lottie
            width={'100%'}
            height={'100%'}
            resizeMode="cover"
            source={require('../../../assets/lotties/lottie-three.json')}
            autoPlay
            loop
          />
        </View>
      </View>
      <View style={styles.bottomView}>
        <View style={styles.bottomTopView}>
          <Text style={styles.signupText}>Sign Up</Text>
          <View style={styles.inputContainer}>
            <View
              style={[
                styles.inputView,
                {
                  marginBottom: SIZE(16),
                },
              ]}>
              <Text style={styles.label}>Email</Text>
              <View style={styles.inputBox}>
                <View style={styles.iconView}>
                  <MailIcon width={'100%'} height={'100%'} />
                </View>
                <TextInput
                  style={styles.input}
                  placeholder="Enter your E-Mail"
                  placeholderTextColor={'#BABABA'}
                />
              </View>
            </View>
            <View
              style={[
                styles.inputView,
                {
                  marginBottom: SIZE(40),
                },
              ]}>
              <Text style={styles.label}>Password</Text>
              <View style={styles.inputBox}>
                <View style={styles.iconView}>
                  <LockIcon width={'100%'} height={'100%'} />
                </View>
                <TextInput
                  style={styles.input}
                  placeholder="Enter your Password"
                  placeholderTextColor={'#BABABA'}
                />
              </View>
            </View>
            <CommanButton title={'Next'} />
            <TouchableOpacity style={styles.accountButton} activeOpacity={0.8}>
              <Text style={styles.accountText}>
                Already have an account?{' '}
                <Text style={styles.highLight}>Log In</Text>
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
};

export default SignUp;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'flex-end',
    paddingBottom: SIZE(35),
  },
  topView: {
    alignItems: 'center',
    marginBottom: SIZE(80),
  },
  lottieView: {
    width: SIZES.WP('60%'),
    height: SIZES.WP('60%'),
    alignItems: 'center',
  },
  bottomView: {
    paddingHorizontal: SIZE(35),
  },
  bottomTopView: {},
  signupText: {
    fontSize: SIZE(20),
    fontFamily: FontFamily.Bold,
    color: '#1879FB',
    marginBottom: SIZE(18),
    paddingLeft: SIZE(20),
  },
  inputContainer: {},
  inputView: {},
  label: {
    fontSize: SIZE(14),
    fontFamily: FontFamily.Regular,
    color: '#000000',
    marginBottom: SIZE(8),
    paddingLeft: SIZE(20),
  },
  inputBox: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: SIZES.WP('12.5%'),
    borderWidth: 1,
    borderColor: '#F1F1F1',
    borderRadius: 8,
    backgroundColor: '#F1F1F1',
    paddingHorizontal: SIZE(8),
  },
  iconView: {
    width: SIZE(20),
    height: SIZE(20),
  },
  input: {
    padding: 0,
    fontSize: SIZE(14),
    fontFamily: FontFamily.Regular,
    color: '#000000',
    width: '70%',
    marginLeft: SIZE(10),
  },
  accountButton: {
    alignItems: 'center',
    marginTop: SIZE(16),
  },
  accountText: {
    fontSize: SIZE(12),
    fontFamily: FontFamily.Semibold,
    color: '#BABABA',
  },
  highLight: {
    color: '#1879FB',
  },
});
