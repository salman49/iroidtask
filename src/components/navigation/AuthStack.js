import {StyleSheet, Text, View} from 'react-native';
import React, {useContext} from 'react';

// packages
import {createNativeStackNavigator} from '@react-navigation/native-stack';

// componets
import Login from '../screens/auth/Login';
import SignUp from '../screens/auth/SignUp';
import Onboarding from '../screens/onboarding/Onboarding';

// context
import {Context} from '../contexts/Store';

const Stack = createNativeStackNavigator();

const AuthStack = () => {
  const {state, dipatch} = useContext(Context);
  return (
    <Stack.Navigator
      initialRouteName="Onboarding"
      screenOptions={{headerShown: false}}>
      {!state.userData.isonBoarding ? (
        <Stack.Screen name="Onboarding" component={Onboarding} />
      ) : (
        <>
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="SignUp" component={SignUp} />
        </>
      )}
    </Stack.Navigator>
  );
};

export default AuthStack;

const styles = StyleSheet.create({});
