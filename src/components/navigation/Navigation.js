import {StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useCallback, useContext} from 'react';

// packages
import {NavigationContainer} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

// components
import AuthStack from './AuthStack';

// context
import {Context} from '../contexts/Store';

const Navigation = () => {
  const {state, dispatch} = useContext(Context);
  useEffect(() => {
    const fetchUserData = async () => {
      const userDataStored = await AsyncStorage.getItem('userdata');
      const userData = JSON.parse(userDataStored);
      dispatch({
        type: 'UPDATE_USER_DATA',
        userData: userData,
      });
    };
    fetchUserData();
  }, []);
  return (
    <NavigationContainer>
      <AuthStack />
    </NavigationContainer>
  );
};

export default Navigation;

const styles = StyleSheet.create({});
