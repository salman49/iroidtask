import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';

// constants
import {SIZE, SIZES, FontFamily} from '../constants/Constants';

const CommanButton = ({title, buttonSubmit}) => {
  return (
    <TouchableOpacity
      style={styles.container}
      activeOpacity={0.8}
      onPress={buttonSubmit}>
      <Text style={styles.titleText}>{title}</Text>
    </TouchableOpacity>
  );
};

export default CommanButton;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: SIZES.WP('12.5%'),
    borderWidth: 1,
    borderColor: '#1879FB',
    borderRadius: 8,
    backgroundColor: '#1879FB',
    paddingHorizontal: SIZE(8),
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleText: {
    fontSize: SIZE(18),
    fontFamily: FontFamily.Semibold,
    color: '#FAFAFA',
  },
});
