// packages
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

// sizes
export const SIZES = {
  WP: wp,
  HP: hp,
};

// fontfamily
export const FontFamily = {
  Bold: 'Poppins-Bold',
  Semibold: 'Poppins-SemiBold',
  Regular: 'Poppins-Regular',
  Light: 'Poppins-Light',
};

// commanSizes

export const SIZE = value => {
  return wp(value / 4.2);
};
