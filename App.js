import {SafeAreaView, StyleSheet, Text, View, StatusBar} from 'react-native';
import React, {useEffect} from 'react';

// navigation
import Navigation from './src/components/navigation/Navigation';

// store
import Store from './src/components/contexts/Store';

const App = () => {
  return (
    <>
      <StatusBar
        animated={true}
        backgroundColor="#FFFFFF"
        barStyle="dark-content"
      />
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <Store>
          <Navigation />
        </Store>
      </SafeAreaView>
    </>
  );
};

export default App;

const styles = StyleSheet.create({});
